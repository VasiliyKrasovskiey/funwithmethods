﻿using System;
using System.Collections.Concurrent;

namespace FunWithMethods
{
    class Program
    {
        public enum Bonus:byte
        {
            manager=1,
            worker=2,
            boss=3,
            economist=4
        }
        static void Main(string[] args)
        {
            Bonus myBonus = Bonus.manager;
            Console.WriteLine($"Perset for {myBonus}={GetPercentOfBonus(myBonus)}");
            Console.WriteLine(myBonus.GetType().Name);
            Console.WriteLine(myBonus.GetType());
            Console.WriteLine(myBonus.ToString());
            Console.WriteLine((byte)myBonus);
            Console.WriteLine(Enum.GetUnderlyingType(myBonus.GetType()));
            Console.WriteLine("---");
            string[] arrStr = new string[3] { "one", "two", "three" };
            ref var str = ref GetArrayElement(arrStr);
            Console.WriteLine(str);
            str = "changeVal";
            foreach(string x in arrStr)
            {
                Console.WriteLine(x);
            }
            Console.WriteLine("---Point---");
            Point p1 = new Point();
            p1.Display();
            p1.Increment();
            p1.Display();
            Point p2;
            p2.x = 12;
            p2.y = 15;
            p2.Increment();
            p2.Display();
        }
        struct Point
        {
            
            public int x;
            public int y;
            public int X 
            {
                get
                {
                    return x;
                }
                set
                {
                    if (value > 10)
                    {
                        x = value;
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
            }
            public int Y
            {
                get
                {
                    return y;
                }
                set
                {
                    if (value>10)
                    {
                        y = value;
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
            }
            public void Increment()
            {
                x++;y++;
            }
            public void Decrement()
            {
                x--;y--;
            }
            public void Display()
            {
                Console.WriteLine($"point X={x}, point Y={y}");
            }
        }
        public static float GetPercentOfBonus(Bonus b)
        {
            float percent = 2;
            switch(b)
            {
                case Bonus.manager:
                    percent=5;
                    break;
                case Bonus.boss:
                    percent = 10;
                    break;
                case Bonus.economist:
                    percent = 6;
                    break;
            }
            return percent;
        }
        //method with ref return
        public static ref string GetArrayElement(string[] arr, int position=0)
        {
            return ref arr[position];
        }
    }

}
